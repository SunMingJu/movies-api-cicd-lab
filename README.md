# Assignment 2 - Agile Software Practice.

Name: MingJu Sun

## API endpoints.

### [Movies](/api/movies/index.js)  
+ /api/movies | GET | Gets a list of movies from loacl.  
+ /api/movies/:id | GET | Gets a single movie's detail from loacl for the movie details page.  
+ /api/movies/:id/reviews | GET | Gets a single movie's reviews from loacl for the movie details page.  
+ /api/movies/tmdb/discover/page:page | GET | Gets a list of movies for the home page.  
+ /api/movies/tmdb/upcoming/page:page | GET | Gets a list of movies for the upcoming movie page.  
+ /api/movies/tmdb/top_rated/page:page | GET | Gets a list of movies for the top rated movie page.  
+ /api/movies/tmdb/movie/:id | GET | Gets a single movie's detail for the movie details page.  
+ /api/movies/tmdb/movie/:id/images | GET | Gets a single movie's images.  
+ /api/movies/tmdb/movie/:id/reviews | GET | Gets a single movie's reviews.  
+ /api/movies/tmdb/movie/:id/movie_credits | GET | Gets a single movie's credits.  

### [People](/api/people/index.js)  
+ /api/people/ | GET | Gets popular people from loacl for popular people page.  
+ /api/people/:id | GET | Gets a single person's detail from loacl for the person details page.  
+ /api/people/tmdb/popular/page:page | GET | Gets popular people for popular people page.  
+ /api/people/tmdb/person/:id | GET | Gets a single person's detail for the person details page.  
+ /api/people/tmdb/person/:id/images | GET | Gets a single person's images.  
+ /api/people/tmdb/person/:id/combined_credits | GET | Gets a single person's combined credits.  


### [Reviews](/api/reviews/index.js)    
All need Authentication.  
+ /api/reviews/movie/:id/reviews | GET | Gets a single movie's reviews both from TMDB and MONGODB.  
+ /api/reviews/movie/:id/reviews/:username | POST | Posts or updates a review from the logged in user for a single movie.  

### [Users](/api/users/index.js)  
+ /api/users/ | GET | Gets all users.  
+ /api/users/ | POST | Registers/Authenticates a user. The body should include username and password.  
+ /api/users/:id | Put | Updates a single user's information.  
+ /api/users/:userName/favourites | POST | Add a single movie to a single user's favourites. The body should include the movie's id.
+ /api/users/:userName/favourites | GET | Gets a single user's all favourite movies.  
+ /api/users/:username/movie/:id/favourites | POST | Delete a speicfic movie from a single user's all favourite movies.

## Test cases.


~~~
  Users endpoint
    GET /api/users
database connected to test on ac-gtghsdk-shard-00-02.94ssq5k.mongodb.net
      √ should return the 2 users and a status 200
    POST /api/users
      For a register action
        when the payload is correct
          √ should return a 201 status and the confirmation message (206ms)
      For an authenticate action
        when the payload is correct
          √ should return a 200 status and a generated token (202ms)
    POST /api/users/:username/favourites
      when the username is valid
        when the movie is not in favourites
          √ should return added message and status code 201 (155ms)
        when the movie is in favourites
          √ return error message and status code 403 (115ms)
      when the username is invalid
        √ return error message and status code 500
    GET /api/users/:username/favourites
      √ should return user's favourite movies list and status code 200
    POST /api/users/:username/movie/:id/favourites
      when the username is valid
        when the movie is in favourites
          √ should return deleted movie message and status code 201 (49ms)
        when the movie is not in favourites
          √ return error message and status cdoe 404
      when the username is invalid
        √ return error message and status code 404

  Movies endpoint
    GET /api/movies
      √ should return 20 movies and a status 200
    GET /api/movies/:id
      when the id is valid
        √ should return the matching movie (145ms)
      when the id is invalid
        √ should return the NOT found message
        √ should return Internal Server Error (215ms)
    GET /api/movies/tmdb/discover/page:page
      when the page is valid
        √ should return the matching movies form tmdb and status code 200 (190ms)
      when the page is invalid
        √ should return the NOT found message
    GET /api/movies/tmdb/upcoming/page:page
      when the page is valid
        √ should return the matching movies form tmdb and status code 200 (109ms)
      when the page is invalid
        √ should return the NOT found message
    GET /api/movies/tmdb/top_rated/page:page
      when the page is valid
        √ should return the matching movies form tmdb and status code 200 (111ms)
      when the page is invalid
        √ should return the NOT found message
    GET /api/movies/tmdb/movie/:id
      when the id is valid
        √ should return the matching movie details from tmdb and status code 200 (98ms)
      when the id is invalid
        √ should return the NOT found message
        √ should return Internal Server Error (187ms)
    GET /api/movies/tmdb/movie/:id/images
      when the id is valid
        √ should return the matching movie images from tmdb and status code 200 (134ms)
      when the id is invalid
        √ should return the NOT found message
        √ should return Internal Server Error (233ms)
    GET /api/movies/tmdb/movie/:id/reviews
      when the id is valid
        √ should return the matching movie reviews from tmdb and status code 200 (139ms)
      when the id is invalid
        √ should return the NOT found message
        √ should return Internal Server Error (232ms)
    GET /api/movies/tmdb/movie/:id/movie_credits
      when the id is valid
        √ should return the matching movie credits from tmdb and status code 200 (119ms)
      when the id is invalid
        √ should return the NOT found message
        √ should return Internal Server Error (191ms)

  People endpoint
    GET /api/people
      √ should return 20 people and status code 200 (64ms)
    GET /api/people/:id
      when the id is valid
        √ should an object of matching people and a status 200
        when the id is invalid
          √ should return the NOT found message
    GET /api/people/tmdb/popular/page:page
      when the page is valid
        √ should return the matching people list form tmdb and status code 200 (127ms)
      when the page is invalid
        √ should return the NOT found message
    GET /api/people/tmdb/person/:id
      when the id is valid
        √ should return the matching person details from tmdb and status code 200 (102ms)
      when the id is invalid
        √ should return the NOT found message
        √ should return Internal Server Error (196ms)
    GET /api/people/tmdb/person/:id/images
      when the id is valid
        √ should return the matching person images from tmdb and status code 200 (108ms)
      when the id is invalid
        √ should return the NOT found message
        √ should return Internal Server Error (197ms)
    GET /api/people/tmdb/person/:id/combined_credits
      when the id is valid
        √ should return the matching person combined credits from tmdb and status code 200 (105ms)
      when the id is invalid
        √ should return the NOT found message
        √ should return Internal Server Error (204ms)

  Reviews endpoint
    GET /api/reviews/movie/:id/reviews
      when movie id is valid
        √ should a object contains a list of the reviews of the movie and a status 200 (120ms)
      when movie id is invalid
        √ should return the NOT found message
        √ should return Internal Server Error (246ms)
    POST /api/reviews/movie/:id/reviews/:username
      when movie id is valid
        when the content is not empty
          √ should return reviews list and status code 201
        when the content is empty
          √ should return error messagea and status code 403
      when movie id is invalid
        √ should return the NOT found message
        

  52 passing (26s)

  ------------------------------------|---------|----------|---------|---------|----------------------
File                                | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s    
------------------------------------|---------|----------|---------|---------|----------------------
All files                           |    76.7 |    53.18 |   86.93 |   77.51 |                      
 movies-api-cicd-lab                |   97.22 |       75 |     100 |   97.14 |                      
  index.js                          |   97.22 |       75 |     100 |   97.14 | 21                   
 movies-api-cicd-lab/api            |   78.37 |       50 |   91.89 |   78.08 |                      
  tmdb-api.js                       |   78.37 |       50 |   91.89 |   78.08 | ...1,125,139,154,168 
 movies-api-cicd-lab/api/movies     |   77.92 |    65.95 |   95.23 |   72.72 |                      
  index.js                          |   76.22 |    65.21 |      95 |   69.66 | ...3,222,261,300,339 
  movieModel.js                     |     100 |      100 |     100 |     100 |                      
  moviesData.js                     |     100 |      100 |     100 |     100 |                      
 movies-api-cicd-lab/api/people     |   88.04 |    67.69 |     100 |   90.74 |                      
  index.js                          |   87.05 |    66.66 |     100 |   89.58 | 16,61,100,139,178    
  peopleModel.js                    |     100 |      100 |     100 |     100 |                      
 movies-api-cicd-lab/api/reviews    |   88.67 |    58.53 |   91.66 |   85.36 |                      
  index.js                          |   89.13 |    56.41 |     100 |   85.71 | 47-52                
  reviewModel.js                    |   85.71 |      100 |      50 |   83.33 | 16                   
 movies-api-cicd-lab/api/users      |   72.93 |    54.94 |   91.17 |   76.13 |                      
  index.js                          |   70.37 |    54.43 |   88.88 |   73.43 | ...1,206-214,294-296 
  userModel.js                      |      84 |    58.33 |     100 |   83.33 | 19,30,34,41          
 movies-api-cicd-lab/authenticate   |   66.66 |    28.57 |      50 |   72.22 |                      
  index.js                          |   66.66 |    28.57 |      50 |   72.22 | 15-20                
 movies-api-cicd-lab/db             |   83.33 |      100 |      50 |   81.81 |                      
  index.js                          |   83.33 |      100 |      50 |   81.81 | 15,18                
 movies-api-cicd-lab/errHandler     |      40 |        0 |       0 |      40 |                      
  index.js                          |      40 |        0 |       0 |      40 | 4-7                  
 movies-api-cicd-lab/initialise-dev |     100 |      100 |     100 |     100 |                      
  movies.js                         |     100 |      100 |     100 |     100 |                      
  users.js                          |     100 |      100 |     100 |     100 |                      
 movies-api-cicd-lab/seedData       |   31.81 |      7.5 |   11.11 |   43.33 |                      
  index.js                          |   21.05 |      7.5 |   11.11 |   29.16 | 12-34,37-38          
  movies.js                         |     100 |      100 |     100 |     100 |                      
  people.js                         |     100 |      100 |     100 |     100 |                      
  users.js                          |     100 |      100 |     100 |     100 |                      
------------------------------------|---------|----------|---------|---------|----------------------
~~~

## Independent Learning (if relevant)

###Coveralls
+ [![Coverage Status](https://coveralls.io/repos/gitlab/SunMingJu/movies-api-cicd-lab/badge.svg?branch=main)](https://coveralls.io/gitlab/SunMingJu/movies-api-cicd-lab?branch=main)
+ https://app.travis-ci.com/SunMingJu/movies-api-cicd-lab.svg?branch=main

I use [Travis-CI](https://app.travis-ci.com/gitlab/SunMingJu/movies-api-cicd-lab/builds/268102775) and [Coveralls](https://coveralls.io/gitlab/SunMingJu/movies-api-cicd-lab), and every time code is committed to GitLab it will be automatically tested by Travis CI, and the results can be sent via a module to Coveralls, which collects this test information, logs the history, and publishes an analysis of the data.

### HEROKU
+ HEROKU Staging App: [https://movie-api-staging-mingjusun-bd2049f5be48.herokuapp.com/](https://movie-api-staging-mingjusun-bd2049f5be48.herokuapp.com/)
+ + HEROKU Production App: [https://asp-ca2-mingjusun-7ff284e26e30.herokuapp.com/](https://asp-ca2-mingjusun-7ff284e26e30.herokuapp.com/)

  
